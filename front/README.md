# Frontend

## How to run

```bash
npm install
npm run serve # webpack dev server+hotreload
```

Technological stack used:

Vue - frontend framework  
Vuex - state manager  
Vuetify - UI library  
axios - xhr library

Adaptive - yes

## Overview

Application is designed to support multiple individual layouts for different routes. For example: login screen and main screen.

To achieve this, intermediate components, also called as views (or pages) were introduced. Each view is lazy loaded on demand by webpack when a specific route is triggered.

Child components are available via the named routes (see: [src/router.js](./src/router.js)).

Please, be aware that some stuff is hard coded. Such as the currently logged in user. Despite the user id is coded it, because login screen hasn't been implemented, user data is still requested from the backend.

## How to work with app

1. Make sure that backend is running locally on port 7070.
2. Navigate to the `/user` url.  
   You will be presented with basic user information that is stored in a database.
3. Press "To review" menu link to get to the `/reviews` page which presents all the reports that a current user has yet to review.
4. Clicking anywhere on the table row will redirect the user to `/reports` page which represents a single Report suitable for review.
5. Write something in a chatbox.

## Todo

- [ ] Login screen
- [ ] Authentication via JWT
- [ ] API key header
- [ ] Admin views with CRUD operations on resources
- [ ] Styles tweaking

import axios from "axios";

const config = {
  apiVersion: "v1",
  apiServer: "http://localhost:7070/",
  options: {
    headers: {
      "Content-Type": "application/json"
    }
  }
};

export default {
  getUserInfo: async id => {
    if (id != "") {
      const endpoint = "/users/" + id;

      try {
        let result = await axios.get(
          config.apiServer + "api/" + config.apiVersion + endpoint,
          config.options
        );
        return result;
      } catch (error) {
        return error;
      }
    }
  },
  getReportById: async reportId => {
    if (reportId) {
      try {
        const endpoint = "/reports/" + reportId;
        const result = await axios.get(
          config.apiServer + "api/" + config.apiVersion + endpoint,
          config.options
        );
        if (result.status == 200) {
          return result.data;
        } else {
          return {};
        }
      } catch (error) {}
    }
  },
  postComment: async (id, commentObj) => {
    let data = {};
    data["reportId"] = id;
    data["comment"] = commentObj;
    console.log("[api] sending comment: ");
    console.log(data);
    const endpoint = "/reports/comment/new";
    const result = await axios.post(
      config.apiServer + "api/" + config.apiVersion + endpoint,
      data,
      config.options
    );
    if (result.status == 200) {
      return { status: "ok" };
    }
  }
};

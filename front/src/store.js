import Vue from "vue";
import Vuex from "vuex";
import api from "./api";
const _ = require("lodash");
import VuexPersist from 'vuex-persist'


Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: 'paypay-review',
  supportCircular: false,
  storage: window.localStorage,
  reducer: state => ({
    user: state.user,
  })
})

export default new Vuex.Store({
  state: {
    user: {
      //@todo This id is hardcoded since there is no
      //@todo login procedure implemetation yet
      id: '5d0b5c0c4a8f423c5c5367dc'
    }
  },
  mutations: {
    GET_USER_INFO: (state, payload) => {
      if (payload) {
        state.user = payload;
      }
      console.dir(state);
    }
  },
  actions: {
    /**
     * Returns user information from the api
     *
     * @param {*} context Context
     * @param {*} userId User ID (not used now)
     */
    GET_USER_INFO: async (context) => {
      //@todo should be func param
      const _userId = context.state.user.id;
      if (_userId != '') {
        const response = await api.getUserInfo(_userId);
        if (response.status == 200) {
          context.commit("GET_USER_INFO", response.data);
        }
      }
    }
  },
  getters: {
    USER: state => {
      return state.user;
    }
  },
  plugins: [vuexLocalStorage.plugin]

});
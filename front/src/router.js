import Vue from "vue";
import Router from "vue-router";
import User from './components/User';
import Report from './components/Report';
import Review from './components/Review';

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [{
    path: '/',
    name: 'main',
    props: true,
    component: () => import( /* webpackChunkName: "mainView" */ './views/MainView.vue'),
    children: [{
        path: 'user',
        components: {
          user: User
        }
      },
      {
        path: 'reports',
        name: 'reports',
        props: true,
        components: {
          reports: Report,
        },
        meta: {
          meta1: 'lol',
          meta2: 'kek'
        }
      },
      {
        path: 'reviews',
        components: {
          reviews: Review
        }
      }

    ]
  }]
});
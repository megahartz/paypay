// api lives here

const express = require('express');
const router = express.Router();
const _ = require('lodash');
const usersRoutes = require('./users');
const reportsRoutes = require('./reports');

router.use(usersRoutes);
router.use(reportsRoutes);

module.exports = router;
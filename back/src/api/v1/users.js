// users api definitions
const express = require("express");
const restError = require("rest-api-errors");
const router = express.Router();
const User = require("../../models/user").User;
const UserModel = new User();

// endpoint: create a new user
router.post("/users/create", async (req, res, next) => {
  if (req.body != "") {
    try {
      let obj = await UserModel.create(req.body);
      res.status(200).json(obj);
      next();
    } catch (error) {
      res.status(500).json(error);
      next(error);
    }
  }
});
// endpoint: returns a single user by Id
router.get("/users/:id", async (req, res, next) => {
  const id = req.params.id;
  if (id !== "" && id !== 'undefined') {
    try {
      let obj = await UserModel.getOne(id);
      if (obj === null) {
        res.status(404).json({});
      } else {
        res.status(200).json(obj);
      }
      next();
    } catch (error) {
      res.status(500).json(error);
      next(error);
    }
  }
});
// endpoint: returns an object that includes results
// query params:
//  page - page number to retrieve
//  limit - max entries per page
router.get("/users", async (req, res, next) => {
  try {
    let result = await UserModel.getall(
      Number(req.query.page),
      Number(req.query.limit)
    );
    if (result.docs.length > 0) {
      res.status(200).json(result);
    } else {
      res.status(404).json({
        status
      });
    }
    next();
  } catch (error) {
    res.json(error);
    next(error);
  }
});
router.patch("/users/:id", async (req, res, next) => {
  try {
    if (req.params.id === "") {
      throw new restError.BadRequest(500, "User ID can't be null");
    }
    const result = await UserModel.update(req.params.id, req.body);
    res.status(200).json(result);
    next();
  } catch (error) {
    res.ststus(400).json(error);
    next(error);
  }
});
router.delete("/users/:id", async (req, res, next) => {
  try {
    if (req.params.id != "") {
      const result = await UserModel.delete(req.params.id);
      if (result.deletedCount !== 0) {
        res.status(200).json({
          status: 200,
          message: "User deleted"
        });
      } else {
        res.status(404).json({
          status: 404,
          message: "User not found"
        });
      }
    }
  } catch (error) {
    next(error);
  }
});

module.exports = router;
// reports api definitions
const _ = require("lodash");
const express = require("express");
const restError = require("rest-api-errors");
const router = express.Router();
const Report = require("../../models/report").Report;
const ReportModel = new Report();

// endpoint: get report by id
router.get("/reports/:id", async (req, res, next) => {
  if (req.params.id !== "") {
    try {
      const report = await ReportModel.get(req.params.id);
      if (!_.isEmpty(report)) {
        res.status(200).json(report);
      } else {
        res.status(404).json({
          message: "Report not found"
        });
      }
      next();
    } catch (error) {
      next(error);
    }
  }
});
router.post("/reports/comment/new", async (req, res, next) => {
  if (req.body) {
    try {
      const result = await ReportModel.postComment(
        req.body.reportId,
        req.body.comment
      );
      console.log(result);
      res.status(200).json(result);
    } catch (error) {
      console.log(error);
      next(error);
    }
  } else {
    res.status(500).json({ status: 500, message: "Empty parameters" });
  }
});

module.exports = router;

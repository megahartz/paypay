# Application Backend

Stack used: express + MongoDB (mongoose). Database is located on mongo cloud.

## Live endpoints

Currently implemented User and Report endpoints. Please, keep in mind that this is not a production code but rather just an example which needs further work to become production ready.

## How to run

```bash
cd src/
npm install
# for Windows PowerShell
$env:PORT=7070; npm start
# bash
PORT=7070 npm start
```

## 'User' resource

Basically CRUD operations.

- `POST /users/create`  
  Creates new user object

- `GET /users/:id`  
  Returns user object by id

- `GET /users/?limit&page`  
  Returns paginated list of all the users  
   `limit` : max entries per page  
   `page` : page number to retrieve

- `PATCH /users/:id`  
  Update user object by id

- `DELETE /users/:id`  
  Delete user object

## 'Report' resource

- `GET /reports/:id`  
  Returns a single report object by id

- `POST /reports/comments/new`  
  Add new comment to report object

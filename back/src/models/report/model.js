// User model
const report = require("./schema");
const restError = require("rest-api-errors");
const mongoose = require("mongoose");
const _ = require("lodash");

/**
 * Report resource
 * @class Report
 */
class Report {
  /**
   * Creates an instance of Report.
   * @memberof Report
   */
  constructor() {
    this._Report = report;
  }
  /**
   * Returns single report object
   *
   * @param {*} id Report ID
   * @memberof Report
   */
  async get(id) {
    if (id !== "") {
      try {
        let result = await this._Report.findById(id);
        if (result === null) {
          return {};
        } else {
          return result;
        }
      } catch (error) {
        console.log(error);
        throw new restError.BadRequest(500, error.message);
      }
    }
  }
  /**
   * Add comment to a report
   *
   * @param {*} reportId Report Id
   * @param {*} commentObj Single comment object
   * @returns MongoDB status object
   * @memberof Report
   */
  async postComment(reportId, commentObj) {
    if (!_.isEmpty(commentObj)) {
      if (
        commentObj.hasOwnProperty("message") &&
        commentObj.message.length < 300
      ) {
        try {
          const result = await this._Report.findOneAndUpdate(
            { _id: reportId },
            { $push: { comments: commentObj } }
          );
          return result;
        } catch (error) {
          return error;
        }
      } else {
        return { status: "error", message: "Maximum message length exceeded" };
      }
    } else {
      return { status: "error", message: "Empty object" };
    }
  }
}

module.exports = {
  Report
};

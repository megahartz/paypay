// Report schema
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const reportSchema = new mongoose.Schema({
    //@description Report owner
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    //@description Report metrics. This is just an example
    metrics: {
        type: Object,
        required: true
    },
    //@description Report comments
    comments: {
        type: Array,
        required: false,
        default: []
    },
    //@description Report creation date
    createdAt: {
        type: Date,
        required: true,
        default: ''
    },
    //@description Report title
    title: {
        type: String,
        required: true,
        default: 'Default report title'
    },

})
reportSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('report', reportSchema);
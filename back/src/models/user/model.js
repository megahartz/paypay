// User model
const _ = require("lodash");
const mongoose = require("mongoose");
const user = require("./schema");
const restError = require("rest-api-errors");
const crypto = require("crypto");

/**
 * User resource
 * @class User
 */
class User {
  /**
   * Creates an instance of User.
   * @memberof User
   */
  constructor() {
    this._User = user;
  }
  /**
   * Creates new user
   *
   * @param {object} userObject User object to create a document from
   * @returns Created user object
   * @memberof User
   */
  async create(userObject) {
    try {
      if (typeof userObject === "object" && !_.isEmpty(userObject)) {
        if (userObject.name != "" || userObject.age != "") {
          if (userObject.passwd !== "") {
            let salt = "_jfru90mvervnow8943_01";
            const encString = userObject.passwd + salt;
            const hash = crypto
              .createHash("sha256")
              .update(encString)
              .digest("base64");
            userObject.passwd = hash;
            // create new document
            let User = new this._User();
            // merge it with the userObject,
            // so you don't have to manually match all the fields (for huge objects)
            User = _.merge(User, userObject);
            // saving document to the database
            let res = await User.save();
            console.log(`[db] created new user: ${res}`);
            return res;
          } else {
            throw new restError.BadRequest(500, "User passwd is required");
          }
        }
      }
    } catch (error) {
      console.error(`[db] failed to create new user: ${error}`);
      // throw new exception for the caller
      throw new restError.BadRequest(500, error.message);
    }
  }
  /**
   * Retrieves single User object from the database
   *
   * @param {string} id Document ID to retrieve
   * @returns {object} Returns a single user document
   * @memberof User
   */
  async getOne(id) {
    let document = {};
    document = await this._User.findById(id).populate("review_list");
    console.log(`[db] returned docs:\n${document}`);
    return document;
  }
  /**
   * Gets all users from the database
   *
   * @param {number} [limit=1] Limit per page
   * @param {number} [page=1] Page number
   * @returns Object that contains the result and metadata
   * @memberof User
   */
  async getall(page = 1, limit = 1) {
    try {
      const options = {
        page: Number(page) || 1,
        limit: Number(limit) || 1
      };
      const documents = await this._User.paginate({}, options);
      console.dir(documents);
      return documents;
    } catch (error) {
      throw new restError.BadRequest(500, error.message);
    }
  }
  /**
   * Updates User object in the database
   *
   * @param {string} id User ID
   * @param {*} userObject User object new values
   * @memberof User
   */
  async update(id, userObject) {
    try {
      if (!_.isEmpty(userObject)) {
        const newUserProps = {};
        for (const p in userObject) {
          newUserProps[p] = userObject[p];
        }
        const result = await this._User.updateOne({
          _id: id
        }, {
          $set: newUserProps
        });
        return result;
      }
    } catch (error) {
      throw new restError.BadRequest(500, error.message);
    }
  }
  async delete(id) {
    if (id != "") {
      try {
        const result = await this._User.deleteOne({
          _id: id
        });
        console.log(`Deleted user: ${id}`);
        console.dir(result);
        return result;
      } catch (error) {
        throw new restError.BadRequest(500, error.message);
      }
    }
  }
}

module.exports = {
  User
};
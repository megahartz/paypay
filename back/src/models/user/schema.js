// User schema
const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    review_list: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "report"
    }],
    passwd: {
        type: String,
        required: true,
    },
    isAdmin: {
        type: Boolean,
        required: false,
        default: false
    }
})
userSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('user', userSchema);
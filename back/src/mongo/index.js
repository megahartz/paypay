// database access
const mongoose = require('mongoose');
const conf = require('../config');

class MongoClient {
    constructor() {
        this.connectString = conf.MONGODB_CONNECT_STRING;
    }
    connect() {
        return mongoose.connect(this.connectString, {
            useNewUrlParser: true
        }, (err) => {
            if (!err) {
                console.log("[db] connected to MongoDB");
            } else {
                console.error(`[db] error connecting to MongoDB: ${err}`)
            }
        })
    }
    disconnect() {
        console.log("[db] closing connection");
        return mongoose.connection.close();
    }
}
module.exports = MongoClient;